require 'rails_helper'

RSpec.describe 'cart requests', type: :request do
  it 'initializes a new cart' do
    get '/cart/new'

    expect(session[:cart]).not_to eq(nil)
  end
end
