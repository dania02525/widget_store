json.extract! widget, :id, :sku, :description, :unit_price, :created_at, :updated_at
json.url widget_url(widget, format: :json)
