# you may use the regular ruby object in models/cart.rb,
# and store it in the session like this session.cart = Cart.new
class CartController < ApplicationController
  def new
    session[:cart] = Cart.new
  end

  def update
  end

  def empty
  end

  def add
  end

  # cart should be totalled and paid for using the
  # services/payment_settlement_service.rb class
  def checkout
  end
end
