class PaymentSettlementService
  class Error < StandardError; end

  def create_charge(amount)
    charger_service(amount)
  end

  def finalize_charge(txn_id)
    finalizer_service(txn_id)
  end

  private

  def charger_service(amount)
    # simulate some external request that may fail here
    sleep 5
    result =
      if rand(10) % 3 == 0
        raise Error.new('aww damn')
      else
        {
          result: :success,
          amount: amount,
          txn_id: "#{rand(10)}-#{rand(10)}-#{rand(10)}"
        }
      end

    # return an unsaved transaction object
    Transaction.new(result)
  end

  def finalizer_service(txn_id)
    send_email
  end

  def send_email(txn_id)
    # find the transaction and send the email
    t = Transaction.find_by(txn_id: txn_id)
    do_send_email(t)
  end

  def do_send_email(txn)
    true # pretend we sent an email
  end
end
