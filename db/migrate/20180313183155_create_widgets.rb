class CreateWidgets < ActiveRecord::Migration[5.1]
  def change
    create_table :widgets do |t|
      t.string :sku
      t.string :description
      t.integer :unit_price

      t.timestamps
    end
  end
end
