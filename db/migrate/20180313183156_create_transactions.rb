class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :txn_id
      t.string :result
      t.integer :amount

      t.timestamps
    end
  end
end
