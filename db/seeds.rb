# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Widget.create(sku: 'test1', description: 'Mega widget', unit_price: 1000)
Widget.create(sku: 'test2', description: 'Mini widget', unit_price: 500)
Widget.create(sku: 'test3', description: 'Medium widget', unit_price: 700)
