# README

## Setup

  Install dependencies:

  ```bash
  $  bundle install
  ```

  Migrate:

  ```bash
  $ bundle exec rake db:migrate
  ```

  Seed the widgets (SKUS test1, test2, test3):

  ```bash
  $ bundle exec rake db:seed
  ```

## Branch and commit

  Create a branch off master with candidate name + last initial:

  ```bash
  $ git checkout -b [first_name][last_initial]
  ```

  Add commits to this branch frequently while interviewing

  ```bash
  $ git add .
  $ git commit -m 'wip'
  $ git push origin [branchname]
  ```


