Rails.application.routes.draw do
  get 'cart/new'

  get 'cart/update'

  get 'cart/empty'

  get 'cart/add'

  get 'cart/checkout'

  resources :widgets
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
